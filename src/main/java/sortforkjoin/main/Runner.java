package sortforkjoin.main;

import java.util.Random;
import java.util.concurrent.ForkJoinPool;

import sortforkjoin.sort.ForkJoinQuickSort;

public class Runner {
	public static void main(String[] args) throws InterruptedException {
		int[] array = new Random().ints(100000).toArray();
		new ForkJoinPool().submit(new ForkJoinQuickSort(array));
	}
}
