package sortforkjoin.sort;

import java.util.concurrent.RecursiveAction;

public class ForkJoinQuickSort extends RecursiveAction {

	private int array[];
	private int lowerIndex;
	private int higherIndex;

	public ForkJoinQuickSort() {
	}

	public ForkJoinQuickSort(int[] array) {
		this.array = array;
		this.lowerIndex = 0;
		this.higherIndex = array.length - 1;
	}

	public ForkJoinQuickSort(int[] array, int lowerIndex, int higherIndex) {
		this.array = array;
		this.lowerIndex = lowerIndex;
		this.higherIndex = higherIndex;
	}

	@Override
	protected void compute() {
		if (array != null && array.length != 0) {
			quickSort(lowerIndex, higherIndex);
		}
	}

	private void quickSort(int lowerIndex, int higherIndex) {
		int i = lowerIndex;
		int j = higherIndex;
		int pivot = array[lowerIndex + (higherIndex - lowerIndex) / 2];
		while (i <= j) {
			while (array[i] < pivot) {
				i++;
			}
			while (array[j] > pivot) {
				j--;
			}
			if (i <= j) {
				exchangeNumbers(i, j);
				i++;
				j--;
			}
		}
		if (lowerIndex < j) {
			new ForkJoinQuickSort(array, lowerIndex, j).fork();
		}
		if (i < higherIndex) {
			new ForkJoinQuickSort(array, i, higherIndex).fork();
		}
	}

	private void exchangeNumbers(int i, int j) {
		int temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}
}
